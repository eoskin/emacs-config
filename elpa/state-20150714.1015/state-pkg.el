(define-package "state" "20150714.1015" "Quick navigation between workspaces" '((emacs "24")) :url "https://github.com/thisirs/state.git" :keywords '("convenience" "workspaces"))
