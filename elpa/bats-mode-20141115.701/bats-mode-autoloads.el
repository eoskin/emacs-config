;;; bats-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (bats-mode) "bats-mode" "bats-mode.el" (21932 32794
;;;;;;  264403 857000))
;;; Generated autoloads from bats-mode.el

(autoload 'bats-mode "bats-mode" "\
Major mode for editing and running Bats tests.

See URL `https://github.com/sstephenson/bats'.

\\{bats-mode-map}

\(fn)" t nil)

(add-to-list 'auto-mode-alist '("\\.bats\\'" . bats-mode))

;;;***

;;;### (autoloads nil nil ("bats-mode-pkg.el") (21932 32794 382344
;;;;;;  619000))

;;;***

(provide 'bats-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; bats-mode-autoloads.el ends here
