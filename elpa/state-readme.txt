This library allows you to switch back and forth between predefined
workspaces. See the README file for more information.

Installation:

(require 'state)

See documentation on https://github.com/thisirs/state#state
