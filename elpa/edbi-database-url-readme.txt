Specify database url with environment variable

    M-x setenv RET DATABASE_URL RET pgsql://me:secret@localhost:5678/test

Connect to you database

    M-x edbi-database-url

Optionally you can specify database url by marking region or type
it interactively.
