;;; sunrise-x-w32-addons-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "sunrise-x-w32-addons" "sunrise-x-w32-addons.el"
;;;;;;  (21959 8147 0 0))
;;; Generated autoloads from sunrise-x-w32-addons.el
 (eval-after-load 'sunrise-commander '(sr-extend-with 'sunrise-x-w32-addons))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; sunrise-x-w32-addons-autoloads.el ends here
