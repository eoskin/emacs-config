;;; gitattributes-whitespace-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "gitattributes-whitespace" "gitattributes-whitespace.el"
;;;;;;  (22025 12529 0 0))
;;; Generated autoloads from gitattributes-whitespace.el

(autoload 'gitattributes-whitespace-apply "gitattributes-whitespace" "\
Apply gitattribute whitespace settings to current buffer.

\(fn)" t nil)

(add-hook 'find-file-hook 'gitattributes-whitespace-apply t)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; gitattributes-whitespace-autoloads.el ends here
