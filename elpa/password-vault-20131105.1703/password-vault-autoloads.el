;;; password-vault-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (password-vault password-vault-register-secrets-file)
;;;;;;  "password-vault" "password-vault.el" (21363 5051 264191 162000))
;;; Generated autoloads from password-vault.el

(autoload 'password-vault-register-secrets-file "password-vault" "\
Load the setq forms to the MODULE to the password-vaults.

\(fn MODULE)" nil nil)

(autoload 'password-vault "password-vault" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("password-vault-pkg.el") (21363 5051 358599
;;;;;;  179000))

;;;***

(provide 'password-vault-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; password-vault-autoloads.el ends here
