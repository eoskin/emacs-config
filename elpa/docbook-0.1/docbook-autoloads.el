;;; docbook-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (docbook-find-file) "docbook" "docbook.el" (21287
;;;;;;  54284 277314 372000))
;;; Generated autoloads from docbook.el

(autoload 'docbook-find-file "docbook" "\
Visit FILENAME as a DocBook document.

\(fn FILENAME)" t nil)

;;;***

;;;### (autoloads nil nil ("docbook-pkg.el") (21287 54284 384578
;;;;;;  918000))

;;;***

(provide 'docbook-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; docbook-autoloads.el ends here
