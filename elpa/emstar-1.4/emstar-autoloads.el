;;; emstar-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (emstar emstar-mode) "emstar" "emstar.el" (21269
;;;;;;  38767 763087 355000))
;;; Generated autoloads from emstar.el

(autoload 'emstar-mode "emstar" "\
Major mode to play emstar.

Commands:
\\{emstar-mode-map}

\(fn)" t nil)

(autoload 'emstar "emstar" "\
Play emstar.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("emstar-pkg.el") (21269 38767 838922 985000))

;;;***

(provide 'emstar-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; emstar-autoloads.el ends here
