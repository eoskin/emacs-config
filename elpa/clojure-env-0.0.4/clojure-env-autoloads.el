;;; clojure-env-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (clojure-env-new-om clojure-env-new-clojurescript)
;;;;;;  "clojure-env" "clojure-env.el" (21696 23328 150047 844000))
;;; Generated autoloads from clojure-env.el

(autoload 'clojure-env-new-clojurescript "clojure-env" "\
Make a new ClojureScript project.

Uses `clojure-env-clojurescript-template' as the name of the
leiningen template to use.

\(fn PROJECT-NAME)" t nil)

(autoload 'clojure-env-new-om "clojure-env" "\
Make a new ClojureScript/OM project.

Uses `clojure-env-om-template' as the name of the leiningen
template to use.

\(fn PROJECT-NAME)" t nil)

;;;***

;;;### (autoloads nil nil ("clojure-env-pkg.el") (21696 23328 276127
;;;;;;  927000))

;;;***

(provide 'clojure-env-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; clojure-env-autoloads.el ends here
