;;; soundklaus-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "soundklaus" "soundklaus.el" (21959 8171 0
;;;;;;  0))
;;; Generated autoloads from soundklaus.el

(autoload 'soundklaus-activities "soundklaus" "\
List activities on SoundCloud.

\(fn)" t nil)

(autoload 'soundklaus-tracks "soundklaus" "\
List all tracks on SoundCloud matching QUERY.

\(fn QUERY)" t nil)

(autoload 'soundklaus-playlists "soundklaus" "\
List all playlists on SoundCloud matching QUERY.

\(fn QUERY)" t nil)

(autoload 'soundklaus-my-playlists "soundklaus" "\
List your playlists on SoundCloud.

\(fn)" t nil)

(autoload 'soundklaus-my-tracks "soundklaus" "\
List your tracks on SoundCloud.

\(fn)" t nil)

(autoload 'soundklaus-desktop-entry-save "soundklaus" "\
Install the SoundKlaus desktop entry for the X Window System.

\(fn)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; soundklaus-autoloads.el ends here
