;;; ssh-config-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (ssh-config-mode) "ssh-config-mode" "ssh-config-mode.el"
;;;;;;  (21660 57527 27635 776000))
;;; Generated autoloads from ssh-config-mode.el

(autoload 'ssh-config-mode "ssh-config-mode" "\
Major mode for fontifiying ssh config files.

\\{ssh-config-mode-map}

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("ssh-config-mode-pkg.el") (21660 57527
;;;;;;  128870 823000))

;;;***

(provide 'ssh-config-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; ssh-config-mode-autoloads.el ends here
