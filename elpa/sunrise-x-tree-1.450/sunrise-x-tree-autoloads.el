;;; sunrise-x-tree-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (sr-tree-mode) "sunrise-x-tree" "sunrise-x-tree.el"
;;;;;;  (21435 53406 614166 120000))
;;; Generated autoloads from sunrise-x-tree.el

(autoload 'sr-tree-mode "sunrise-x-tree" "\
Tree view for the Sunrise Commander file manager.

\(fn)" t nil)
 (eval-after-load 'sunrise-commander '(sr-extend-with 'sunrise-x-tree))

;;;***

;;;### (autoloads nil nil ("sunrise-x-tree-pkg.el") (21435 53406
;;;;;;  686197 357000))

;;;***

(provide 'sunrise-x-tree-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; sunrise-x-tree-autoloads.el ends here
