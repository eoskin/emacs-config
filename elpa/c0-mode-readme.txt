   This mode implements font-locking and indentation of C0 based on
   the functionality provided by cc-mode. C0 is a variant of C
   developed at Carnegie Mellon University for pedagogical
   purposes.

   To automatically use c0-mode when visiting a file with
   extensions ".c0" or ".h0", add the following to your init.el:

   (add-to-list 'auto-mode-alist '("\.[ch]0$" . c0-mode))

   Note: The interface used in this file requires CC Mode 5.30 or
   later.
