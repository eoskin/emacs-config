;;; flymake-jshint-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (flymake-jshint-load) "flymake-jshint" "flymake-jshint.el"
;;;;;;  (21381 43426 504659 870000))
;;; Generated autoloads from flymake-jshint.el

(autoload 'flymake-jshint-load "flymake-jshint" "\
Configure flymake mode to check the current buffer's JavaScript syntax.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("flymake-jshint-pkg.el") (21381 43426
;;;;;;  587387 328000))

;;;***

(provide 'flymake-jshint-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; flymake-jshint-autoloads.el ends here
