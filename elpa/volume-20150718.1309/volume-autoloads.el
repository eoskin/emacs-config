;;; volume-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "volume" "volume.el" (21959 8129 0 0))
;;; Generated autoloads from volume.el

(autoload 'volume "volume" "\
Tweak your sound card volume.

\(fn)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; volume-autoloads.el ends here
