;;; eshell-did-you-mean-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "eshell-did-you-mean" "eshell-did-you-mean.el"
;;;;;;  (22001 1599 0 0))
;;; Generated autoloads from eshell-did-you-mean.el

(autoload 'eshell-did-you-mean-output-filter "eshell-did-you-mean" "\
\"Did you mean\" filter for eshell OUTPUT.
Should be added to `eshell-preoutput-filter-functions'.

\(fn OUTPUT)" nil nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; eshell-did-you-mean-autoloads.el ends here
