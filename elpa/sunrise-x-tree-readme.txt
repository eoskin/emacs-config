   Tree View for the Sunrise Commander file manager.

This package is an enhancement for the Sunrise Commander, which is a
two-pane file manager, like Midnight Commander or Total Commander.

It provides a directories-only tree view that can be used for fast
navigation, as well as for several basic operations on files and
directories. It uses the excellent "tree-widget.el" library written by
David Ponce and works the same in text consoles as well as in
graphical environments, using either the mouse or just the keyboard.
