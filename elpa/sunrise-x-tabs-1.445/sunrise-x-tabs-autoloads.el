;;; sunrise-x-tabs-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "sunrise-x-tabs" "sunrise-x-tabs.el" (21435
;;;;;;  53400 442166 60000))
;;; Generated autoloads from sunrise-x-tabs.el
 (eval-after-load 'sunrise-commander '(sr-extend-with 'sunrise-x-tabs))

;;;***

;;;### (autoloads nil nil ("sunrise-x-tabs-pkg.el") (21435 53400
;;;;;;  598835 537000))

;;;***

(provide 'sunrise-x-tabs-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; sunrise-x-tabs-autoloads.el ends here
