;;; ssh-tunnels-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (ssh-tunnels) "ssh-tunnels" "ssh-tunnels.el" (21660
;;;;;;  58489 49723 477000))
;;; Generated autoloads from ssh-tunnels.el

(autoload 'ssh-tunnels "ssh-tunnels" "\
View and manipulate SSH tunnels.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("ssh-tunnels-pkg.el") (21660 58489 164091
;;;;;;  654000))

;;;***

(provide 'ssh-tunnels-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; ssh-tunnels-autoloads.el ends here
