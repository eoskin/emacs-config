;;; grails-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (grails-mode) "grails-mode" "grails-mode.el" (21910
;;;;;;  4498 561249 887000))
;;; Generated autoloads from grails-mode.el

(autoload 'grails-mode "grails-mode" "\
Grails Minor Mode.
     With no argument, this command toggles the mode.
     Non-null prefix argument turns on the mode.
     Null prefix argument turns off the mode.

     When Grails mode is enabled, several keys are enabled that
     will allow navigation around a typical grails project.

\(fn &optional ARG)" t nil)

;;;***

;;;### (autoloads nil nil ("grails-mode-pkg.el") (21910 4498 669744
;;;;;;  810000))

;;;***

(provide 'grails-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; grails-mode-autoloads.el ends here
