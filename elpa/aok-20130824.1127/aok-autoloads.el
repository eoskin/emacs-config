;;; aok-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "aok" "aok.el" (21588 39035 0 0))
;;; Generated autoloads from aok.el

(autoload 'all-occur "aok" "\
Search all buffers for REXP.

\(fn REXP)" t nil)

(autoload 'type-occur "aok" "\
EXTENSION denotes a filetype extension to search.
Run occur in all buffers whose names match this type for REXP.

\(fn EXTENSION REXP)" t nil)

(autoload 'mode-occur "aok" "\
Search all buffers with major mode MODE for REXP.

\(fn MODE REXP)" t nil)

(autoload 'occur-select "aok" "\
select what you wan't to see occur

\(fn MORE REGX &optional NOTHING)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; aok-autoloads.el ends here
