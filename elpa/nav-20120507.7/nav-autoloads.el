;;; nav-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "ack" "ack.el" (21582 65020 121521 29000))
;;; Generated autoloads from ack.el

(defvar ack-history nil)

;;;***

;;;### (autoloads (nav) "nav" "nav.el" (21582 65020 173521 30000))
;;; Generated autoloads from nav.el

(autoload 'nav "nav" "\
Opens Nav in a new window to the left of the current one.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("nav-pkg.el") (21582 65020 257356 963000))

;;;***

(provide 'nav-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; nav-autoloads.el ends here
