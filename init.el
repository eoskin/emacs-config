;;; init.el --- Summary:
;;; Commentary:
;;; Add custom options

;;; Code:
;; Package-Requires: ((multi-web-mode))
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")
                         ("ELPA" . "http://tromey.com/elpa/")
                         ("SC" . "http://joseito.republika.pl/sunrise-commander/")))


(defmacro when-gui (&rest body)
  "Works just like `progn' but will only evaluate expressions in VAR when Emacs is running in a terminal else just nil."
  `(when (display-graphic-p) ,@body))

(when-gui
    (desktop-save-mode 1))

;; enable linum mode
(global-linum-mode 1)

;; column numbers mode
(column-number-mode 1)

;; I hate tabs!
(setq-default indent-tabs-mode nil)

;; load the packaged named xyz.
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(calendar-date-style (quote iso))
 '(calendar-week-start-day 1)
 '(coffee-tab-width 2)
 '(column-number-mode t)
 '(custom-enabled-themes (quote (solarized-light)))
 '(custom-safe-themes
   (quote
    ("d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" default)))
 '(dired-subtree-use-backgrounds nil)
 '(doc-view-continuous t)
 '(flycheck-coffee-coffeelint-executable "coffeelint")
 '(flycheck-coffee-executable "coffee")
 '(global-git-gutter-mode t)
 '(gradle-mode 1)
 '(guru-global-mode 0)
 '(js3-indent-level 4)
 '(keyboard-coding-system (quote utf-8))
 '(org-agenda-files (quote ("~/Dropbox/")))
 '(save-place t nil (saveplace))
 '(show-paren-mode t)
 '(tool-bar-mode nil)
 '(uniquify-buffer-name-style (quote forward) nil (uniquify))
 '(add-to-list 'eshell-preoutput-filter-functions
               #'eshell-did-you-mean-output-filter)
 ;; Use more italics
 '(setq solarized-use-more-italic t)
 ;; Use less colors for indicators such as git:gutter, flycheck and similar
 '(setq solarized-emphasize-indicators nil)
 '(when (memq window-system (quote (mac ns))) t))
 ;; disable not emacs key binds

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Meslo LG S" :foundry "bitstream" :slant normal :weight normal :height 98 :width normal))))
 '(dired-subtree-depth-1-face ((t (:background "white smoke"))))
 '(dired-subtree-depth-3-face ((t (:background "#212627"))))
 '(dired-subtree-depth-4-face ((t (:background "#1e2223"))))
 '(dired-subtree-depth-5-face ((t nil)))
 '(dired-subtree-depth-6-face ((t (:background "#1a191a")))))


(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; hooks for programs source files
(add-hook 'prog-mode-hook 'undo-tree-mode)
(add-hook 'prog-mode-hook 'hs-minor-mode)
(add-hook 'prog-mode-hook 'flycheck-mode)
(add-hook 'prog-mode-hook 'highlight-parentheses-mode)
(add-hook 'prog-mode-hook 'company-mode)
(add-hook 'prog-mode-hook 'fic-mode)
(add-hook 'prog-mode-hook 'company-mode)
(add-hook 'prog-mode-hook 'turn-on-eldoc-mode)

;; hooks for cmake files
(add-hook 'cmake-mode-hook 'highlight-parentheses-mode)
(add-hook 'cmake-mode-hook 'company-mode)

;; hooks for restclient files
(add-hook 'restclient-mode-hook 'company-mode)

;; hooks for py files
(add-hook 'python-mode-hook 'eldoc-mode)
(add-hook 'python-mode-hook 'anaconda-mode)

;; hooks for rst files
(add-hook 'rst-mode-hook 'flycheck-mode)
(add-hook 'rst-mode-hook 'highlight-parentheses-mode)
(add-hook 'rst-mode-hook 'hs-minor-mode)
(add-hook 'rst-mode-hook 'undo-tree-mode)

;; hooks for yaml files
(add-hook 'yaml-mode-hook 'undo-tree-mode)

;; hooks for html files
(add-hook 'html-hook 'flycheck-mode)
(add-hook 'html-hook 'undo-tree-mode)
(add-hook 'html-hook 'hs-minor-mode)

;; hooks for js files
(add-hook 'js-mode-hook 'js3-mode)
(add-hook 'js-mode-hook 'undo-tree-mode)
(add-hook 'js3-mode-hook 'undo-tree-mode)

;; Coffee-mode tab spaces

;; hooks for conf files
(add-hook 'conf-mode-hook 'undo-tree-mode)

;; hooks for nginx files
(add-hook 'nginx-mode-hook 'undo-tree-mode)

;; hooks for gradle files
(add-to-list 'auto-mode-alist '("\\.gradle\\'" . groovy-mode))

;; hooks for html files
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))

(put 'upcase-region 'disabled nil)

(global-set-key (kbd "C-x C-b") 'ibuffer)
(autoload 'ibuffer "ibuffer" "List buffers." t)

;; turn on easy switch buffers
(windmove-default-keybindings 'meta)

;; set scroll step in lines
(setq scroll-step 1)

(global-set-key (kbd "C-c /") 'company-complete)
(global-unset-key (kbd "C-q"))
(global-set-key (kbd "C-q C-u") 'string-inflection-all-cycle)

(package-initialize)
(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))

;;; init.el ends here
(put 'downcase-region 'disabled nil)

(eval-after-load "company"
  '(progn
     (add-to-list 'company-backends 'company-anaconda)
     (add-to-list 'company-backends 'company-restclient)))
